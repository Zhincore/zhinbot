import { ActivitiesModule } from "./activities";
import { MessageRatingModule } from "./messagerating";
import { ModeratorModule } from "./moderator";
import { PlayerModule } from "./player";
import { SelfRolesModule } from "./selfroles";
import { UtilsModule } from "./utils";

export default [ActivitiesModule, MessageRatingModule, ModeratorModule, PlayerModule, SelfRolesModule, UtilsModule];
